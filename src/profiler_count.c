#include <glib.h>		// for ghashtable
#include <unistd.h>		//for close

//for open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "profiler.h"
#include "to_flamegraph_format.h"

static void
record_trace_in_table (formated_buf * f_buf, GHashTable * table)
{

  char *trace;

  extract_formated_trace_from_buffer (f_buf, &trace);
  insert_a_trace_in_the_table (&trace, table);

  return;
}

static void
stack_traces_to_file (GHashTable * table)
{

  int fd;

  if (s_data.output_file == NULL)
    fd = open (DEFAULT_OUTPUT_FILE,
	       O_WRONLY | O_CREAT | O_TRUNC | O_APPEND, S_IRWXU);
  else
    fd = open (s_data.output_file,
	       O_WRONLY | O_CREAT | O_TRUNC | O_APPEND, S_IRWXU);

  if (fd == -1)
    perror ("agent error open");
  else
    {
      g_hash_table_foreach (table, print_all_table, &fd);
      close (fd);
    }

  return;
}

void
profiler_count_process (read_buf * r_buf, GHashTable * table,
			jvmtiEnv * jvmti)
{

  formated_buf f_buf;		//carries formated traces for flamegraph 
  method_data m_data;		//hold current method_data
  int num_frames = 0, index_frame = -1;

  init_formated_buf (&f_buf);
  init_method_data (&m_data);

  num_frames = read_int_from_buffer (r_buf);

  if (num_frames < -11 || num_frames > (int) MAX_FRAME_NUMBER)
    {
      /* these values should not be possible,
       * but still happen when multiple sighandler happens before calling this function
       */
      record_unknown (&f_buf, MET_DEL);
    }
  else if (num_frames > 0)
    {

      while (++index_frame < (num_frames))
	{

	  if (fill_method_data (r_buf, &m_data) == -1)
	    {
	      record_unknown (&f_buf, MET_DEL);
	      break;
	    }

	  if (m_data.id != NULL)
	    {
	      if (!get_info_from_jvmti
		  (jvmti, &f_buf, &m_data, SIG_DEL, MET_DEL))
		{
		  record_unknown (&f_buf, MET_DEL);
		}
	    }
	  else
	    {
	      //No method id could be found : it should have not been created yet
	      record_unknown (&f_buf, MET_DEL);
	    }

	}
    }
  else if (num_frames < 0)
    {

      char *error_name;

      error_name = get_error_name (num_frames);
      insert_a_trace_in_the_table (&error_name, table);	//error_name get free in this function
      free (f_buf.buf);

      return;

    }
  record_trace_in_table (&f_buf, table);
  free (f_buf.buf);

  return;
}

void
profiler_count_end (GHashTable * table)
{

  stop_itimer ();
  stack_traces_to_file (table);

  return;
}
