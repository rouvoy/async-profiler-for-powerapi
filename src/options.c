#include <string.h>		//for strtok
#include <stdio.h>		//for perror
//for exit
#include <unistd.h>
#include <stdlib.h>

#include "max_frame_number.h"
#include "shared_data.h"

#define INTERVAL_IN_US 15000

void
parse_options (char *options)
{
  char *current_option;
  const char delimiter = ',';

  //s_data.output_file = NULL;
  s_data.interval = INTERVAL_IN_US;
  s_data.options_flag = 0;
  s_data.max_frame_number = 0;
  s_data.start_time = 0;

  if (options == NULL)
    return;

  current_option = strtok (options, &delimiter);

  do
    {

      if (s_data.output_file == NULL
	  && strncmp (current_option, "file=", 5) == 0)
	{

	  int size_of_file_name;

	  current_option += 5;

	  size_of_file_name = strlen (current_option) + 1;	//+1 for '\0'
	  if (size_of_file_name == 1)
	    {
	      perror
		("agent wrong argument : file option must receive a file name");
	      exit (0);
	    }
	  s_data.output_file = malloc (size_of_file_name);
	  memcpy (s_data.output_file, current_option, size_of_file_name);
	}
      else if (strcmp (current_option, "smart_watts") == 0)
	{
	  s_data.options_flag |= SMART_WATTS;
	}
      else if (strcmp (current_option, "continuous_logging") == 0)
	{
	  s_data.options_flag |= CONTINUOUS_LOGGING;
	}
      else if (strcmp (current_option, "jit_level") == 0)
	{
	  s_data.options_flag |= JIT_LEVEL;
	}
      else if (strncmp (current_option, "interval=", 9) == 0)
	{
	  current_option += 9;
	  s_data.interval = atoi (current_option);
	  if (s_data.interval <= 0)
	    {
	      perror
		("agent wrong argument : interval must be superior to 0");
	      exit (0);
	    }
	}
      else if (strncmp (current_option, "start_time=", 11) == 0)
	{
	  current_option += 11;
	  s_data.start_time = atoi (current_option);
	  if (s_data.start_time < 0)
	    {
	      perror
		("agent wrong argument : start time can not be negative");
	      exit (0);
	    }
	}
      else if (strncmp (current_option, "max_frame_number=", 17) == 0)
	{
	  current_option += 11;
	  s_data.max_frame_number = atoi (current_option);
	  if (s_data.max_frame_number <= 0)
	    {
	      printf
		("agent wrong argument : max_frame_number can't be inferior or equal to 0.");
	      exit (0);
	    }
	  else if (s_data.max_frame_number > MAX_FRAME_NUMBER)
	    {
	      printf
		("agent wrong argument : max frame number is superior to the "
		 "limit of what a pipe can handle.");
	      exit (0);
	    }
	}
      else
	{
	  printf ("unrecognize option : %s\n", current_option);
	  exit (0);
	}
      
    }
  while ((current_option = strtok (NULL, &delimiter)) != NULL);

  if ((s_data.options_flag & CONTINUOUS_LOGGING)
      && !(s_data.options_flag & SMART_WATTS))
    {
      fprintf (stderr,
	       "Warning : coninuous_logging option isn't yet implemented without smart_watts option. Option will be ignored.");
    }
  
  return;
}
