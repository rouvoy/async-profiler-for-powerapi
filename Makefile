#-fno-tree-loop-distribute-patterns

LIB = -I /usr/include/glib-2.0/ -I /usr/lib64/glib-2.0/include -I /usr/lib/x86_64-linux-gnu/glib-2.0/ -I /usr/lib/x86_64-linux-gnu/glib-2.0/include -I ./lib

SRC = src/agent.c src/profiler.c src/profiler_count.c src/profiler_smartwatts.c src/options.c src/profiler_process_common.c src/to_flamegraph_format.c src/sigprof_handler.c src/memory_getter.c src/continuous_logger.c

OPTIONS = -Wall -fpic -shared -pthread -O2

LINKED_LIBRARY = -lglib-2.0

all : dir
	gcc $(OPTIONS) $(LIB) $(SRC) $(LINKED_LIBRARY) -o bin/agent.so

dir:
	mkdir -p bin
